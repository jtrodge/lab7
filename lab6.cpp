/*
Jason Rodgers
CPSC 1021, Section 002, F20
jtrodge@clemson.edu
Nushrat Humaira
*/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));
	int i;

  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
  employee empArr[10];
  for (i = 0; i < 10; i++) {
		cout << "Employee #" << i+1 << endl;
		cout << endl;
    cout << "What is the last name of the employee?  ";
    cin >> empArr[i].lastName;
    cout << "What is the first name of the employee?  ";
    cin >> empArr[i].firstName;
    cout << "When was the employee born?  ";
    cin >> empArr[i].birthYear;
    cout << "What is the hourly wage of the employee?  ";
    cin >> empArr[i].hourlyWage;
		cout << endl;
  }
		cout << endl;

  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
   random_shuffle(empArr, empArr+10, myrandom);

   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/
		employee newArr[5] = {empArr[0], empArr[1], empArr[2], empArr[3], empArr[4]};


    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
		 sort(newArr, newArr+5, name_order);


    /*Now print the array below */
		for (employee finalArr : newArr) {
			cout << setw(6) << right << finalArr.lastName + ", " + finalArr.firstName << endl;
			cout << endl;
			cout << setw(6) << right << finalArr.birthYear << endl;
			cout << endl;
			cout << setw(7) << setprecision(2) << fixed << showpoint << right << finalArr.hourlyWage << endl;
			cout << endl;
		}


  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
	if (lhs.lastName < rhs.lastName) {
		return true;
	}
	else {
		return false;
	}
}
